import coloredlogs
import logging


def get_logger(name="Default"):
    """ Return a logger set with the name in parameter, "Default" if missing

    :param name: name of the logger (optional)
    :return: logger instance
    """
    logger = logging.getLogger(name)
    coloredlogs.install(level=logging.DEBUG,
                        show_hostname=False,
                        show_timestamps=False)
    return logger
