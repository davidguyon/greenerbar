#!/bin/bash
echo "##########"
echo "Pleiades "
echo

echo "> received argument:"
echo "  - " $1
echo

#echo "> extract files in $1"
#start=$(($(date +%s%N)/1000000))
#tar -xzvf $1 -C ./
#end=$(($(date +%s%N)/1000000))
#echo "> extracted in " $((end-start)) "ms"
#echo

echo "> execute mJPEG"
start=$(($(date +%s%N)/1000000))

mJPEG -blue  ~/DSS2B.fits -1s 99.999% gaussian-log \
    i -green ~/DSS2R.fits -1s 99.999% gaussian-log \
      -red  ~/DSS2IR.fits -1s 99.999% gaussian-log \
      -out DSS2_BRIR.jpg

end=$(($(date +%s%N)/1000000))
echo "> mJPEG executed in " $((end-start)) "ms"
echo
