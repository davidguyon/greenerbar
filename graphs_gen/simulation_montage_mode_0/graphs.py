import numpy as np
import matplotlib.pyplot as plt
import json, time, os
from datetime import datetime, timedelta

os.environ['TZ'] = 'Europe/Amsterdam'
time.tzset()
print time.tzname

#path = "/root/greenerbar/graphs_gen/montage1_montage2_0/"
path = "./"

def open_file(filename):
    with open(path + filename) as f:
        return json.load(f)

def open_turning_on_data():
    with open(path + "turning_on_data.log") as f:
        return [line.strip() for line in f]

## Power
json_power_data = open_file("power_consumption.log")
turn_on_date = open_file("turn_on_date.log")
turn_off_date = open_file("turn_off_date.log")
turning_on_data = open_turning_on_data()

max_width = 5500

plt.figure(figsize=(20,8), dpi=600)

zone1 = 'taurus-5'
zone2 = 'taurus-9'
controller_node = 'taurus-4'
puppet_master = 'taurus-2'

def translate_zone_to_host(zone):
    if zone == 'zone1': return zone1
    elif zone == 'zone2': return zone2
    else: return None

def translate_host_to_generic_name(host):
    if host == zone1: return 'compute1'
    elif host == zone2: return 'compute2'
    elif host == controller_node: return 'controller'
    elif host == puppet_master: return 'puppet master'
    else: return 'unknown host'

def get_host_color(host):
    if host == zone1: return 'b'
    elif host == zone2: return 'r'
    else: return 'y'

def get_previous_turn_off_date(host,timestamp):
    previous_turn_off = None
    for t in turn_off_date[host]:
        if (previous_turn_off == None and t <= timestamp) or \
            (previous_turn_off < t and t <= timestamp):
            previous_turn_off = t
    return previous_turn_off

def get_previous_turn_on_date(host,timestamp):
    previous_turn_on = 0
    for t in turn_on_date[host]:
        if (previous_turn_on == None and t <= timestamp) or \
            (previous_turn_on < t and t <= timestamp):
            previous_turn_on = t
    return previous_turn_on

plt.title('Simulation Montage (normal mode)')
plt.ylabel('power (Watt)')

total_joule = 0
start_time = None
for entry in json_power_data:
    host = entry['host']
    data = entry['data']
    start_time = int(time.mktime(datetime.strptime(data[0]['timestamp'], "%Y-%m-%d %H:%M:%S").timetuple()))

    # Compute nodes
    if host == zone1:
        print('dealing with Compute node:' + host)
        x = []
        y = []

        for element in data:
            timestamp = int(time.mktime(datetime.strptime(element['timestamp'], "%Y-%m-%d %H:%M:%S").timetuple()))
            yval = element['value'] - 100 if element['value'] - 100 > 0 else 0
            y.append(yval)
            x.append(timestamp-start_time)

        plt.plot(x,y,get_host_color(host),label=translate_host_to_generic_name(host))
        joule = np.trapz(y,x)
        print translate_host_to_generic_name(host)+ ' consumed ' +  str(joule) +' Joules'
        total_joule += joule

print('Total joules: ' + str(total_joule) + ' Joules')
print(len(x))
watt = total_joule / x[len(x)-1]
print('Total watt: ' + str(watt))

plt.plot((4195, 4195), (10, 80), 'k-')
plt.plot((4263, 4263), (10, 80), 'k-')
plt.plot((4797, 4797), (10, 80), 'k-')

plt.xlim(0, max_width)

plt.grid(True)

plt.xlabel('time (s)')
plt.tight_layout()
plt.show()
#plt.savefig("power.png")
