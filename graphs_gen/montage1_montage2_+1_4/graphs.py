import numpy as np
import matplotlib.pyplot as plt
import json, time, os
from datetime import datetime, timedelta

os.environ['TZ'] = 'Europe/Amsterdam'
time.tzset()
print time.tzname

#path = "/root/greenerbar/graphs_gen/montage1_montage2_+1/"
path = "./"

def open_file(filename):
    with open(path + filename) as f:
        return json.load(f)

def open_turning_on_data():
    with open(path + "turning_on_data.log") as f:
        return [line.strip() for line in f]

## Power
json_power_data = open_file("power_consumption.log")
turn_on_date = open_file("turn_on_date.log")
turn_off_date = open_file("turn_off_date.log")
turning_on_data = open_turning_on_data()

## Montage 1
montage1 = []
montage1.append(open_file("montage-workflow1/cpu_util-stepA_0.log"))
montage1.append(open_file("montage-workflow1/cpu_util-stepA_1.log"))
montage1.append(open_file("montage-workflow1/cpu_util-stepA_2.log"))
montage1.append(open_file("montage-workflow1/cpu_util-stepB_0.log"))

## Montage 2
montage2 = []
montage2.append(open_file("montage-workflow2/cpu_util-stepA_0.log"))
montage2.append(open_file("montage-workflow2/cpu_util-stepA_1.log"))
montage2.append(open_file("montage-workflow2/cpu_util-stepA_2.log"))
montage2.append(open_file("montage-workflow2/cpu_util-stepB_0.log"))

# Workflows
workflows = []
workflows.append(montage1)
workflows.append(montage2)

nb_subplots = len(workflows)+1
max_width = 785

plt.figure(figsize=(11.69,8.27), dpi=100)

zone1 = 'taurus-3'
zone2 = 'taurus-16'
zone3 = 'taurus-8'
zone4 = 'taurus-9'
controller_node = 'taurus-15'
puppet_master = 'taurus-11'

def translate_zone_to_host(zone):
    if zone == 'zone1': return zone1
    elif zone == 'zone2': return zone2
    elif zone == 'zone3': return zone3
    elif zone == 'zone4': return zone4
    else: return None

def translate_host_to_generic_name(host):
    if host == zone1: return 'compute1'
    elif host == zone2: return 'compute2'
    elif host == zone3: return 'compute3'
    elif host == zone4: return 'compute4'
    elif host == controller_node: return 'controller'
    elif host == puppet_master: return 'puppet master'
    else: return 'unknown host'

def get_host_color(host):
    if host == zone1: return 'b'
    elif host == zone2: return 'r'
    elif host == zone3: return 'g'
    elif host == zone4: return 'm'
    else: return 'y'

def get_previous_turn_off_date(host,timestamp):
    previous_turn_off = None
    for t in turn_off_date[host]:
        if (previous_turn_off == None and t <= timestamp) or \
            (previous_turn_off < t and t <= timestamp):
            previous_turn_off = t
    return previous_turn_off

def get_previous_turn_on_date(host,timestamp):
    previous_turn_on = 0
    for t in turn_on_date[host]:
        if (previous_turn_on == None and t <= timestamp) or \
            (previous_turn_on < t and t <= timestamp):
            previous_turn_on = t
    return previous_turn_on

## Power
#
#
plt.subplot(nb_subplots, 1, 1)
plt.title('Power vs CPU util')
plt.ylabel('Power in Watt')

total_joule = 0
start_time = None
for entry in json_power_data:
    host = entry['host']
    data = entry['data']

    start_time = data[0]['timestamp']

    x = []
    y = []

    start_time = int(time.mktime(datetime.strptime(data[0]['timestamp'], "%Y-%m-%d %H:%M:%S").timetuple()))

    # Compute nodes
    if host == zone1 or host == zone2 or host == zone3 or host == zone4:
        print('dealing with Compute node:' + host)
        turning_on = False

        for element in data:
            timestamp = int(time.mktime(datetime.strptime(element['timestamp'], "%Y-%m-%d %H:%M:%S").timetuple()))
            nearest_turn_off = get_previous_turn_off_date(host,timestamp)
            nearest_turn_on = get_previous_turn_on_date(host,timestamp)

            if nearest_turn_on == timestamp-2:
                print "-2 ok"
            if nearest_turn_on == timestamp-1:
                print "-1 ok"
            if nearest_turn_on == timestamp:
                print "0 ok"
            if nearest_turn_on == timestamp+1:
                print "+1 ok"
            if nearest_turn_on == timestamp+2:
                print "+2 ok"

            if nearest_turn_on == timestamp:
                turning_on = True
                plt.plot((timestamp-start_time, timestamp-start_time), (20, 180), 'k-')
            elif turning_on and (nearest_turn_on + 150) <= timestamp:
                turning_on = False
                plt.plot((timestamp-start_time, timestamp-start_time), (20, 180), 'k-')

            if nearest_turn_off != None and nearest_turn_off > nearest_turn_on:
                y.append(8) # 8 Watts when turned off
                #print timestamp-start_time
            elif turning_on:
                index = timestamp - nearest_turn_on
                y.append(int(float(turning_on_data[index])))
            else:
                y.append(element['value'])

            x.append(timestamp-start_time)

    # Controller node
    elif host == controller_node:
        print('ignore Controller node:' + host)
        continue

    # Puppet Master node
    elif host == puppet_master:
        print('ignore Puppet Master node:' + host)
        continue

    # Error
    else:
        print("Don't know host " + host)

    plt.plot(x,y,get_host_color(host),label=translate_host_to_generic_name(host))
    joule = np.trapz(y,x)
    print translate_host_to_generic_name(host)+ ' consumed ' +  str(joule) +' Joules'
    total_joule += joule

print('Total energy: ' + str(total_joule/3600) + 'Wh')
watt = total_joule / max(x)
print('Total watt: ' + str(watt))

plt.xlim(0, max_width)

plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
           ncol=5, mode="expand", borderaxespad=0.)

plt.grid(True)

## CPU util
#
#
index = 2
for workflow in workflows:
    durations = list()
    print("Handling Montage %d" % (index-1))

    plt.subplot(nb_subplots, 1, index)
    plt.grid(True)
    plt.ylabel('Montage ' + str(index-1) + ': CPU util')
    index += 1
    for step in workflow:
        x = []
        y = []
        color = get_host_color(translate_zone_to_host(step['zone']))
        for element in step['data']:
            updated_date = datetime.strptime(element['timestamp'], "%Y-%m-%dT%H:%M:%S") + timedelta(hours=2)
            timestamp = int(time.mktime(updated_date.timetuple()))
            x.append(timestamp-start_time)
            y.append(element['value'])
        plt.plot(x,y,color+'--',label=step['job']+'_'+str(step['index']),markersize=3)

        duration = max(x) - min(x)
        durations.append(duration)
        print(" duration %s: %d sec" % (step['job'], duration))
    print(" total duration: %d" % (max(durations[0:3]) + durations[3]))

    plt.xlim(0, max_width)
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
               ncol=4, mode="expand", borderaxespad=0.)

plt.xlabel('time (s)')
plt.tight_layout()
plt.subplots_adjust(hspace = .4)
plt.savefig("power.png")
