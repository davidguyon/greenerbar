#!/bin/bash
echo "##########"
echo "Palmtree  "
echo

echo "> run PALMTREE with given config file"
mkdir output
start_main=$(($(date +%s%N)/1000000))
mpirun -np $(nproc) ~/greenie-project/workflows/palmtree/PALMTREE $1
end_main=$(($(date +%s%N)/1000000))
mv output/*.h5 .
echo "> script executed in " $((end_main-start_main)) "ms"
