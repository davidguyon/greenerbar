$src_dir="%(test_user_dir)s/palmtree-workflow"
Exec { path => "/usr/bin:/usr/sbin/:/bin:/sbin:/usr/local/bin:/usr/local/sbin" }

notify { "friedatest-notification":
	message=>"Installing test app",
}

package { ['openmpi-bin','cmake','g++','libopenmpi-dev','libhdf5-openmpi-dev','libhdf5-dev']:
	ensure=>installed,
}
