#!/bin/bash
echo "> execute mArchive on DSS2B"

start_DSS2B=$(($(date +%s%N)/1000000))

mkdir DSS2B;
cd DSS2B;
mkdir raw projected;
cd raw;

mArchiveList dss DSS2B "$1 $2" $3 $3 tmp_DSS2B.tbl;
sed "1,7d" tmp_DSS2B.tbl > remote.tbl;
rm tmp_DSS2B.tbl;
mArchiveExec remote.tbl;

end_DSS2B=$(($(date +%s%N)/1000000))
echo "> mArchive DSS2B executed in " $((end_DSS2B-start_DSS2B)) "ms"
echo

cd .. ;

echo "> execute mImgtbl(1) on DSS2B"
start_DSS2B=$(($(date +%s%N)/1000000))
mImgtbl raw rimages.tbl;
end_DSS2B=$(($(date +%s%N)/1000000))
echo "> mImgtbl(1) DSS2B executed in " $((end_DSS2B-start_DSS2B)) "ms"
echo

echo "> execute mProjExec on DSS2B"
start_DSS2B=$(($(date +%s%N)/1000000))
#mProjExec -p raw rimages.tbl ../pleiades.hdr projected stats.tbl;
python ~/mProjectPara.py
end_DSS2B=$(($(date +%s%N)/1000000))
echo "> mProjExec DSS2B executed in " $((end_DSS2B-start_DSS2B)) "ms"
echo

echo "> execute mImgtbl(2) on DSS2B"
start_DSS2B=$(($(date +%s%N)/1000000))
mImgtbl projected pimages.tbl;
end_DSS2B=$(($(date +%s%N)/1000000))
echo "> mImgtbl(2) DSS2B executed in " $((end_DSS2B-start_DSS2B)) "ms"
echo

echo "> execute mAdd on DSS2B"
start_DSS2B=$(($(date +%s%N)/1000000))
mAdd -p projected pimages.tbl ../pleiades.hdr DSS2B.fits;
echo "> mAdd DSS2B executed in " $((end_DSS2B-start_DSS2B)) "ms"
echo

mv DSS2B.fits ..;

cd .. ;
