#!/bin/bash
echo "> execute mArchive on DSS2R"

start_DSS2R=$(($(date +%s%N)/1000000))

mkdir DSS2R;
cd DSS2R;
mkdir raw projected;
cd raw;

mArchiveList dss DSS2R "$1 $2" $3 $3 tmp_DSS2R.tbl;
sed "1,7d" tmp_DSS2R.tbl > remote.tbl;
rm tmp_DSS2R.tbl;
mArchiveExec remote.tbl;

end_DSS2R=$(($(date +%s%N)/1000000))
echo "> mArchive DSS2R executed in " $((end_DSS2R-start_DSS2R)) "ms"
echo

cd .. ;

echo "> execute mImgtbl(1) on DSS2R"
start_DSS2R=$(($(date +%s%N)/1000000))
mImgtbl raw rimages.tbl;
end_DSS2R=$(($(date +%s%N)/1000000))
echo "> mImgtbl(1) DSS2R executed in " $((end_DSS2R-start_DSS2R)) "ms"
echo

echo "> execute mProjExec on DSS2R"
start_DSS2R=$(($(date +%s%N)/1000000))
#mProjExec -p raw rimages.tbl ../pleiades.hdr projected stats.tbl;
python ~/mProjectPara.py
end_DSS2R=$(($(date +%s%N)/1000000))
echo "> mProjExec DSS2R executed in " $((end_DSS2R-start_DSS2R)) "ms"
echo

echo "> execute mImgtbl(2) on DSS2R"
start_DSS2R=$(($(date +%s%N)/1000000))
mImgtbl projected pimages.tbl;
end_DSS2R=$(($(date +%s%N)/1000000))
echo "> mImgtbl(2) DSS2R executed in " $((end_DSS2R-start_DSS2R)) "ms"
echo

echo "> execute mAdd on DSS2R"
start_DSS2R=$(($(date +%s%N)/1000000))
mAdd -p projected pimages.tbl ../pleiades.hdr DSS2R.fits;
echo "> mAdd DSS2R executed in " $((end_DSS2R-start_DSS2R)) "ms"
echo

mv DSS2R.fits ..;

cd .. ;
