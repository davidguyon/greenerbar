#!/bin/bash
echo "> execute mArchive on DSS2IR"

start_DSS2IR=$(($(date +%s%N)/1000000))

mkdir DSS2IR;
cd DSS2IR;
mkdir raw projected;
cd raw;

mArchiveList dss DSS2IR "$1 $2" $3 $3 tmp_DSS2IR.tbl;
sed "1,7d" tmp_DSS2IR.tbl > remote.tbl;
rm tmp_DSS2IR.tbl;
mArchiveExec remote.tbl;

end_DSS2IR=$(($(date +%s%N)/1000000))
echo "> mArchive DSS2IR executed in " $((end_DSS2IR-start_DSS2IR)) "ms"
echo

cd .. ;

echo "> execute mImgtbl(1) on DSS2IR"
start_DSS2IR=$(($(date +%s%N)/1000000))
mImgtbl raw rimages.tbl;
end_DSS2IR=$(($(date +%s%N)/1000000))
echo "> mImgtbl(1) DSS2IR executed in " $((end_DSS2IR-start_DSS2IR)) "ms"
echo

echo "> execute mProjExec on DSS2IR"
start_DSS2IR=$(($(date +%s%N)/1000000))
#mProjExec -p raw rimages.tbl ../pleiades.hdr projected stats.tbl;
python ~/mProjectPara.py
end_DSS2IR=$(($(date +%s%N)/1000000))
echo "> mProjExec DSS2IR executed in " $((end_DSS2IR-start_DSS2IR)) "ms"
echo

echo "> execute mImgtbl(2) on DSS2IR"
start_DSS2IR=$(($(date +%s%N)/1000000))
mImgtbl projected pimages.tbl;
end_DSS2IR=$(($(date +%s%N)/1000000))
echo "> mImgtbl(2) DSS2IR executed in " $((end_DSS2IR-start_DSS2IR)) "ms"
echo

echo "> execute mAdd on DSS2IR"
start_DSS2IR=$(($(date +%s%N)/1000000))
mAdd -p projected pimages.tbl ../pleiades.hdr DSS2IR.fits;
echo "> mAdd DSS2IR executed in " $((end_DSS2IR-start_DSS2IR)) "ms"
echo

mv DSS2IR.fits ..;

cd .. ;
