#!/bin/bash
echo "##########"
echo "Pleiades "
echo

echo "> received scripts:"
echo "  - " $1

start_main=$(($(date +%s%N)/1000000))

echo "> generate hdr file for coordinates [" $2 ";" $3 "] and " $4 " degrees wide"
mHdr "$2 $3" $4 tmp.hdr
sed "1,7d" tmp.hdr > pleiades.hdr
rm tmp.hdr
echo

echo "> content of pleiades.hdr"
cat pleiades.hdr
echo

echo "> run the script in background"
nohup bash $1 $2 $3 $4 >> ~/scriptA.log &
pids[0]=$!

echo "> wait for the script to terminate"
for pid in ${pids[*]};
  do wait $pid;
done;
end_main=$(($(date +%s%N)/1000000))
echo "> script " $1 " executed in " $((end_main-start_main)) "ms"

mv *.fits /data/output/