$src_dir="%(test_user_dir)s/my-test"
Exec { path => "/usr/bin:/usr/sbin/:/bin:/sbin:/usr/local/bin:/usr/local/sbin" }

notify { "friedatest-notification":
		message=>"Installing test app",
	}
	
	
package { ['python','python-dev','python-setuptools','python-pip','git','subversion','gcc']:
	ensure=>installed,
}

# This is not specific to this installation and should probably be put in a place
# where it can be reused.
define install_pkg ($pkgname, $timeout= 1200, $extra_pip_args = "") {
  exec {
   "InstallPkg_$pkgname":
   command => "pip install $extra_pip_args $pkgname",
   timeout => $timeout,
   require => Package["python-pip"];
  }
}

# Install friedatest app
install_pkg { "install_coloredlogs_def":
   pkgname=>"coloredlogs",
   timeout=>12000,
   require=>Package['python-pip'],
}
