import multiprocessing
import threading
import os
import os.path

# Get number of CPU
nb_cpu = multiprocessing.cpu_count()

# Initialize folders
INPUT_DIR = "./raw/"
OUTPUT_DIR = "./projected/"
HDR_FILE = "../pleiades.hdr"

# Get files
files = [name for name in os.listdir(INPUT_DIR)
         if os.path.isfile(os.path.join(INPUT_DIR, name))]
print(files)
files.remove("remote.tbl")
nb_files = len(files)


def mproject(cpu, _files):
    for file in _files:
        print("Running mProject for file " + file + " on CPU " + str(cpu))
        os.system("taskset -c " + str(cpu) + " mProject " + INPUT_DIR +
                  file + " " + OUTPUT_DIR + file + " " + HDR_FILE)


threads = list()

for x in range(0, nb_cpu):
    start = (nb_files / nb_cpu) * x
    end = (nb_files / nb_cpu) * (x + 1)
    thread_files = [file for index, file in enumerate(files)
                    if start <= index < end]

    print("CPU " + str(x) + ": " + str(len(thread_files)) + " files")

    t = threading.Thread(
        target=mproject,
        args=[x, thread_files]
    )
    threads.append(t)
    t.start()

# Wait for all threads to finish
for t in threads:
    t.join()
