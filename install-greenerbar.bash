#!/bin/bash

# Install doxygen, graphviz, python-dev, virtualenv, pip, ipython and tig
echo "Install python3-pip, tree and tig"
apt-get install --yes python3-pip tree tig
# Doxygen has been removed from the installation 
# apt-get install doxygen graphviz if needed
echo
echo "Install virtualenv and ipython"
pip3 install virtualenv ipython
echo

# Create virtualenv, activate it and install package on it
echo "Create virtualenv, activate it and install package on it"
virtualenv env
source env/bin/activate
pip install -r requirements.txt
echo

# Configuration of git
echo "Configuration of git"
git config --global user.name "David Guyon"
git config --global user.email "david@guyon.me"
git config --global push.default matching
echo git config --global user.name
echo git config --global user.email
echo

# Configuration of doxypy
#echo "Configuration of doxypy"
#cd doxypy
#python setup.py install
#echo
