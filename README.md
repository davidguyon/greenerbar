 GreenerBar

## Installation

When you OpenStack is deployed on your nodes, copy the installation script on the controller:

    scp install-greenerbar.bash root@host:greenerbar

Now connect to your controller and execute the script.

    ./install-greenerbar.bash

It will install the needed packages, clone the project, prepare the virtualenv, activate it and install the needed
Python packages.

Go back to your local machine and use the `sshfs` command to mount the _greenerbar_ folder from the controller to your
local machine:

    sshfs root@host:greenerbar greenerbar

In my case I write my Python code with PyCharm (free licence). Execute the PyCharm on your local machine and open the
project. Make sure you are using the Python environment. To check `Ctrl+Alt+s` > Python interpreter > Add local >
select the greenerbar/env/bin/python interpreter.

On the controller side, to execute the program I advice you to use the `ipython` command (installed with
_install-greenerbar.bash_). Run `ipython` and then type `run main.py`. You have access to the variables for debug
purpose.

## How to start the program

Go in the _greenerbar_ folder and then `source env/bin/activate` to enable the Python virtual environment and also `source testrc` to enable to OpenStack authentication variables. Now you can start `ipython` and `run main.py`. The program starts and asks you to specify the first workflow data. Give the path to the workflow description file (YAML) and the execution mode to use. Next you can add more workflows or launch the execution. 

## Some useful documentations

There are no official **complete** documentations, except to this one:
[Python Novaclient doc](http://docs.openstack.org/developer/python-novaclient/ref/v2/). So you will probably need to
look into the [source code of python-novaclient](https://github.com/openstack/python-novaclient/) and find the URL that
matches with the ones listed on the [OpenStack API documentation](http://developer.openstack.org/api-ref.html).

If I find other good links to share, I'll update this section.

## Troubleshooting

### Missing dependencies

If you encounter any problem because of missing dependencies, it is because the Python packages are not found. They are
installed in the Python virtualenv. Just activate it to fix it:

    source env/bin/activate

