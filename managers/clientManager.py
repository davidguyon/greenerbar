# @package clientManager
# Documentation of the Client Manager package
#

import os
import sys
import logging
from novaclient.client import Client as NovaClient

import my_logger

logger = my_logger.get_logger('ClientManager')


class ClientManager:

    def __init__(self):
        """ Constructor of ClientManager

        :return:
        """
        if not ClientManager._check_environ_variables():
            logger.error("Execute `source adminrc`")
            sys.exit("Failed at ClientManager")

        # OpenStack API version
        self.VERSION = 2
        # OpenStack username used to connect to the API
        self.USERNAME = os.getenv('OS_USERNAME')
        # OpenStack password used to connect to the API
        self.PASSWORD = os.getenv('OS_PASSWORD')
        # OpenStack API authentication URL
        self.AUTH_URL = os.getenv('OS_AUTH_URL')
        # OpenStack project ID
        self.PROJECT_ID = os.getenv('OS_TENANT_NAME')

        # Reduce number of logs emit from 'connectionpool'
        logging.getLogger('requests').setLevel(logging.WARNING)

        # local instance of the Client
        self.client = NovaClient(self.VERSION, self.USERNAME, self.PASSWORD,
                                 self.PROJECT_ID, self.AUTH_URL)

    @staticmethod
    def _check_environ_variables():
        """ Check if OpenStack environment variables are set

        :return: True if all env variables exist, False otherwise
        """
        if "OS_USERNAME" not in os.environ:
            logger.error("Env variable missing: OS_USERNAME")
            return False
        if "OS_PASSWORD" not in os.environ:
            logger.error("Env variable missing: OS_PASSWORD")
            return False
        if "OS_AUTH_URL" not in os.environ:
            logger.error("Env variable missing: OS_AUTH_URL")
            return False
        if "OS_TENANT_NAME" not in os.environ:
            logger.error("Env variable missing: OS_TENANT_NAME")
            return False

        return True
