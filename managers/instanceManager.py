# @package instanceManager
# Documentation of the Instance Manager package
#

import my_logger

logger = my_logger.get_logger('InstanceManager')


class InstanceManager:

    def __init__(self, client):
        """ Constructor of InstanceManager

        :param client: Nova client given by the ClientManager
        :return:
        """

        # local instance of the Client
        self.client = client

    def get_all_instances(self):
        """ Return the list of running instances

        :return: list of running instances
        """
        return self.client.servers.list()

    def get_instance(self, instance_id):
        """ Return the instance details

        :param instance_id: id of an instance
        :return: details of the instance matching the given id
        """
        return self.client.servers.get(instance_id)

    def create_instance(self, name, image, flavor, availability_zone=None,
                        keypair=None, sec_groups=None, script=None):
        """ Return the new created instance

        :param name: Name of the instance to create in a String format
        :param image: OpenStack image to deploy on the virtual machine
        :param flavor: OpenStack flavor to use for the virtual machine
        :param availability_zone: zone to create the instance (Optional)
        :param keypair: Name of the keypair to copy on the instance (Optional)
        :param sec_groups: security groups to link to the instance (Optional)
        :param script: Post installation script to execute (Optional)
        :return: created instance
        """
        logger.debug("create_instance")

        network = self.client.networks.find(label="private")

        try:
            logger.info("Creating new instance...")
            instance = self.client.servers.create(
                name=name,
                image=image,
                flavor=flavor,
                availability_zone=availability_zone,
                key_name=keypair,
                security_groups=sec_groups,
                nics=[{'net-id': network.id}],
                userdata=script
            )
            return instance
        except Exception as e:
            logger.error(e)
            return None

    def delete_instance(self, instance_id):
        """ Delete the instance with the id given in parameter

        :param instance_id: id of the instance to delete
        :return:
        """
        instance = self.get_instance(instance_id)
        instance.delete()

    @staticmethod
    def show_instance_info(instance):
        """ Print details of the instance in parameter

        :param instance: Existing OpenStack instance
        :return:
        """
        print("Instance: " + instance.name)
        print("\tStatus: " + str(instance.status))
        print("\tIP: " + str(instance.networks))
        print("\tID: " + str(instance.id))

    @staticmethod
    def pause(instance):
        """ Pause the instance in parameter

        :param instance: Existing and unpaused OpenStack instance
        :return:
        """
        try:
            instance.pause()
        except Exception as e:
            logger.error(e)

    @staticmethod
    def unpause(instance):
        """ Unpause the instance in parameter

        :param instance: Existing and paused OpenStack instance
        :return:
        """
        try:
            instance.unpause()
        except Exception as e:
            logger.error(e)

    @staticmethod
    def shelve(instance):
        """ Shelve the instance in parameter

        :param instance: Existing and unshelved OpenStack instance
        :return:
        """
        try:
            instance.shelve()
        except Exception as e:
            logger.error(e)

    @staticmethod
    def unshelve(instance):
        """ Unshelve the instance in parameter

        :param instance: Existing and shelved OpenStack instance
        :return:
        """
        try:
            instance.unshelve()
        except Exception as e:
            logger.error(e)
