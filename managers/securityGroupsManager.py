# @package securityGroupsManager
# Documentation of the Security Groups Manager package
#

import my_logger

logger = my_logger.get_logger('SecurityGroupsManager')


class SecurityGroupsManager:

    def __init__(self, client):
        """ Constructor of SecurityGroupsManager

        :param client: Nova client given by the ClientManager
        :return:
        """

        # local instance of the Client
        self.client = client

    def get_all_security_groups(self):
        """ Return the list of all security groups

        :return: list of security groups
        """
        return self.client.security_groups.list()
