# @package keypairsManager
# Documentation of the Keypairs Manager package
#

import my_logger

logger = my_logger.get_logger('KeypairsManager')


class KeypairsManager:

    def __init__(self, client):
        """ Constructor of KeypairsManager

        :param client: Nova client given by the ClientManager
        :return:
        """

        # local instance of the Client
        self.client = client

    def get_all_keypairs(self):
        """ Return the list of keypairs

        :return: list of keypairs
        """
        return self.client.keypairs.list()
