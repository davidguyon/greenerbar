# @package postInstallScriptsManager
# Documentation of the Post Install Script Manager package
#

import os
import socket

import common
import my_logger

logger = my_logger.get_logger('PostInstallScriptsManager')


class PostInstallScriptsManager:

    def __init__(self):
        """ Constructor of PostInstallScriptsManager

        :return:
        """
        self.currentScript = ""

    def add(self, cmd):
        self.currentScript += cmd
        self.currentScript += "\n"

    def clear(self):
        self.currentScript = ""

    def add_bash_header(self):
        self.add("#!/bin/bash")

    def add_cd_home_debian(self):
        self.add("cd /home/debian")

    def add_fix_ssh_security(self):
        self.add("sudo ssh-keygen -A")

    def add_install_killall(self):
        self.add("sudo apt-get install --yes psmisc")

    def add_upload_ssh_key(self):
        id_rsa = self.get_id_rsa()
        self.add("echo '" + id_rsa + "' > .ssh/upload.rsa")
        self.add("chmod 600 .ssh/upload.rsa")

    def add_root_ssh_key(self):
        ssh_key = self.get_ssh_key()
        self.add("echo '" + ssh_key + "' > .ssh/key.pem")
        self.add("chmod 600 .ssh/key.pem")

    def add_mprojectpara(self):
        m_project_para = self.get_mprojectpara()
        self.add("echo '" + m_project_para + "' > mProjectPara.py")

    def add_controller_ip(self):
        ip = self.get_controller_ip()
        self.add("echo 'CONTROLLER_IP=" + ip + "' >> /etc/environment")

    def add_scp_from_ips(self, ips=None):
        if ips is not None:
            for ip in ips:
                self.add("scp -i .ssh/key.pem " +
                         "-oStrictHostKeyChecking=no debian@" + str(ip) +
                         ":/data/output/*.fits /home/debian/")

    def add_netcat(self, port):
        ip = self.get_controller_ip()
        self.add("hostname | netcat " + ip + " " + str(port))

    def get_montage_script(self, port, ips=None):
        """ Return the post installation script for montage

        :param port: port to use for the netcat command
        :param ips: array of IP addresses used for the scp command (optional)
        :return: bash script for Montage
        """
        self.clear()
        self.add_bash_header()
        self.add_cd_home_debian()
        self.add_install_killall()
        self.add_fix_ssh_security()
        self.add_upload_ssh_key()
        self.add_root_ssh_key()
        self.add_mprojectpara()
        self.add_controller_ip()
        self.add_scp_from_ips(ips)
        self.add_netcat(port)

        return self.currentScript

    def get_palmtree_script(self, port):
        """ Return the post installation script for palmtree

        :param port: port to use for the netcat command
        :return: bash script for palmtree
        """
        self.clear()
        self.add_bash_header()
        self.add_cd_home_debian()
        self.add_install_killall()
        self.add_fix_ssh_security()
        self.add_upload_ssh_key()
        self.add_netcat(port)

        return self.currentScript

    def get_netcat_script(self, port):
        """ Return the post installation script for netcat

        :param port: port to use for the netcat command
        :return: bash script for netcat
        """
        self.clear()
        self.add_bash_header()
        self.add_install_killall()
        self.add_fix_ssh_security()
        self.add_netcat(port)

        return self.currentScript

    @staticmethod
    def get_id_rsa():
        """ Return the content of /home/upload/.ssh/id_rsa

        :return: content of /home/upload/.ssh/id_rsa
        """
        if not os.path.exists("/home/upload/.ssh/id_rsa"):
            raise Exception("id_rsa not found")
        try:
            file = open("/home/upload/.ssh/id_rsa", "r")
            return file.read()
        except Exception as e:
            logger.error(e)

    @staticmethod
    def get_ssh_key():
        """ Return the content of ssh_key.pem

        :return: content of ssh_key.pem
        """
        if not os.path.exists("/root/ssh_key.pem"):
            raise Exception("ssh_key not found")
        try:
            file = open("/root/ssh_key.pem", "r")
            return file.read()
        except Exception as e:
            logger.error(e)

    @staticmethod
    def get_controller_ip():
        """ Return the controller's IP address

        :return: controller's IP address
        """
        try:
            return socket.gethostbyname(socket.gethostname())
        except Exception as e:
            logger.error(e)

    @staticmethod
    def get_mprojectpara():
        """ Return the content of mProjectPara.py

        :return: content of mProjectPara.py
        """
        if not os.path.exists(common.PATH + "mProjectPara.py"):
            raise Exception("mProjectPara.py not found")
        try:
            file = open(common.PATH + "mProjectPara.py", "r")
            return file.read()
        except Exception as e:
            logger.error(e)
