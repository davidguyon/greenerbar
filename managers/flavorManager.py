# @package flavorManager
# Documentation of the Flavor Manager package
#

import my_logger

logger = my_logger.get_logger('FlavorManager')


class FlavorManager:

    def __init__(self, client):
        """ Constructor of FlavorManager

        :param client: Nova client given by the ClientManager
        :return:
        """

        # local instance of the Client
        self.client = client

    def get_all_flavors(self):
        """ Return the list of flavors

        :return: list of flavors
        """
        return self.client.flavors.list()

    def get_specific_flavor(self, flavor_id):
        """ Return details of a specific flavor
        :param flavor_id: id of an existing OpenStack flavor

        :return: details of a specific flavor
        """
        try:
            flavor = self.client.flavors.get(flavor=flavor_id)
            return flavor
        except Exception as e:
            logger.error(e)
            return None

    @staticmethod
    def get_flavor_name(flavor):
        """ Return the name of the flavor

        :param flavor: OpenStack flavor object
        :return: name of the flavor
        """
        return flavor.name

    @staticmethod
    def get_cpu(flavor):
        """ Return the required number of CPU for the flavor

        :param flavor: OpenStack flavor object
        :return: required number of CPU for the flavor
        """
        return flavor.vcpus

    @staticmethod
    def get_ram(flavor):
        """ Return the required amount of RAM for the flavor

        :param flavor: OpenStack flavor object
        :return: required amount of RAM for the flavor
        """
        return flavor.ram

    @staticmethod
    def get_disk(flavor):
        """ Return the required amount of disk for the flavor
        :param flavor: OpenStack flavor object
        :return: required amount of disk for the flavor
        """
        return flavor.disk

    @staticmethod
    def show_flavor_info(flavor):
        """ Print details of the flavor in parameter

        :param flavor: OpenStack flavor object
        :return:
        """
        print("Flavor: " + flavor.name)
        print("\tCPU: " + str(flavor.vcpus))
        print("\tMemory: " + str(flavor.ram))
        print("\tHDD: " + str(flavor.disk))

    def which_flavor_for_job(self, job, green):
        """ Find the best suitable flavor depending on the amount of
        hardware required by the job in parameter and also take into
        consideration the value of the "green" parameter which can be -1,
        0 or 1.

        :param job: instance of a Job
        :param green: amount of "green" for the flavor selection
        :return: best suitable flavor for the job in parameter
        """
        logger.debug("which_flavor_for_job")
        all_flavors = self.get_all_flavors()

        # Index of the optimal flavor
        optimal = None

        # Index of the selected flavor
        selected = None

        # Avoid unset parameters
        cpu = 0 if job.get_cpu() is None else job.get_cpu()
        ram = 0 if job.get_ram() is None else job.get_ram()
        disk = 0 if job.get_disk() is None else job.get_disk()

        # Find optimal flavor (level 0)
        for index, flavor in enumerate(all_flavors):
            # If optimal not set and flavor resources >= job's needs
            if self.get_cpu(flavor) >= cpu and \
               self.get_ram(flavor) >= ram and \
               self.get_disk(flavor) >= disk and \
               optimal is None:
                optimal = index

        # No optimal flavor found
        if optimal is None:
            logger.error("Can't find an optimal flavor. You're asking too much"
                         " resources")
            logger.error("Biggest flavor has been selected")
            optimal = len(all_flavors) - 1

        # Select best flavor depending on green value
        if green == 0:
            selected = optimal
        elif green == -1:
            selected = (optimal - 1) if optimal > 0 \
                else optimal
        elif green == 1:
            selected = (optimal + 1) if optimal < (len(all_flavors) - 1) \
                else optimal

        selected_flavor = all_flavors[selected]
        logger.info(job.get_name() + ": green=" + str(green) + " optimal=" +
                    str(optimal) + " selected=" + str(selected) + " ==> " +
                    str(selected_flavor))

        return selected_flavor
