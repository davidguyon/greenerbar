# @package powerManager
# Documentation of the Power Manager package
#

import requests
from datetime import datetime

import my_logger

logger = my_logger.get_logger('PowerManager')


class PowerManager:

    @staticmethod
    def get_power_consumption(job_id, start_time=None, end_time=None):
        """ Get the power consumption data for the job in parameter

        :param job_id: id of the job deployed on Grid'5000
        :param start_time: starting time for the range of
                           power consumption data (opetional)
        :param end_time: end time for the range of power
                         consumption data (optional)
        :return: array of power consumption data
        """
        url = "https://api.grid5000.fr/sid/sites/lyon/metrics/power/" \
              "timeseries/?job_id=" + str(job_id)
        resp = requests.get(url, verify=False)

        if resp.status_code != 200:
            logger.error('Kwapi answered with the status code ' +
                         str(resp.status_code))
            return

        # Parse the response in json
        obj = resp.json()

        to_return = []

        for item in obj['items']:
            host = item['uid']
            data = []
            size_timestamps = len(item['timestamps'])
            size_values = len(item['values'])
            if size_timestamps != size_values:
                logger.warning("Number of timestamps and values "
                               "should be the same")

            for index in range(0, size_timestamps-1):
                if start_time is not None and \
                        item['timestamps'][index] < start_time:
                    continue
                elif end_time is not None and \
                        item['timestamps'][index] > end_time:
                    continue
                else:
                    timestamp = datetime.fromtimestamp(
                            (float(item['timestamps'][index])*1000) / 1e3)
                    data.append({
                        'timestamp': str(timestamp),
                        'value': item['values'][index]
                    })
            to_return.append({
                'host': host,
                'data': data
            })

        return to_return
