# @package aggregatesManager
# Documentation of the Aggregates Manager package
#

import my_logger

logger = my_logger.get_logger('AvailabilityZonesManager')


class AggregatesManager:

    def __init__(self, client):
        """ Constructor of AggregatesManager

        :param client: Nova client given by the ClientManager
        :return:
        """

        # local instance of the Client
        self.client = client

    def create_aggregate(self, name):
        """ Create an aggregate named with the name in parameter

        :param name: Name of the aggregate
        :return:
        """
        return self.client.aggregates.create(name, name)

    def get_all_aggregates(self):
        """ Return the list of aggregates

        :return: list of aggregates
        """
        return self.client.aggregates.list()

    def add_host_to_aggregate(self, aggregate, host):
        """ Add host to the aggregate in parameter

        :param aggregate: Aggregate on which to add the host
        :param host: Host to add to the aggregate
        :return:
        """
        self.client.aggregates.add_host(aggregate, host)

    @staticmethod
    def get_hosts_from_zone(aggregate):
        """ Return the list of hosts on this aggregate

        :param aggregate: Aggregate on which to get the list of hosts
        :return: list of hosts on this aggregate
        """
        return aggregate.hosts

    def initiate_zones(self, host_manager):
        """ Create one zone/aggregate per host found in the nova zone

        :param host_manager: HostManager instance to get all hosts
        :return:
        """
        all_hosts = host_manager.get_hosts()
        if len(all_hosts) > 0:
            logger.info("Creating zones")
            index = 1
            for host in all_hosts:
                aggregate = self.create_aggregate("zone" + str(index))
                self.add_host_to_aggregate(aggregate, str(host.host_name))
                index += 1
