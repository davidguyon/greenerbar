# @package logsManager
# Documentation of the Logs Manager package
#

import os
import json

import common
import my_logger

logger = my_logger.get_logger('LogsManager')


class LogsManager:

    def __init__(self):
        """ Constructor of LogsManager

        :return:
        """
        logger.info("Logs manager is ready")

    @staticmethod
    def create_cpu_util_log_file(workflow_name, job_name, zone,
                                 instance_id, data, index=None):
        """ Create a log file which contains the CPU util data

        :param workflow_name: name of the workflow
        :param job_name: name of the job in the workflow (can be "master")
        :param zone: zone used by the instance
        :param instance_id: id of the instance used by the job or by master
        :param data: data to save into the log file
        :param index: index of the worker for this job (optional if master)
        :return:
        """
        workflow_path = common.WORKFLOWS_PATH + workflow_name

        if job_name == "master":
            os.makedirs(workflow_path + "/logs_history/master/")
            path = workflow_path + "/logs_history/" + job_name + \
                "/cpu_util-" + job_name + ".log"
        else:
            path = workflow_path + "/logs_history/" + job_name + \
                "/cpu_util-" + job_name + "_" + str(index) + ".log"

        json_data = {
            'workflow': workflow_name,
            'job': job_name,
            'index': index,
            'zone': zone,
            'instance': str(instance_id),
            'type': 'cpu util',
            'data': data
        }

        with open(path, 'w+') as f:
            json.dump(json_data, f, indent=2)

    @staticmethod
    def create_power_consumption_log_file(data):
        """ Create a log file which contains the power consumption of all hosts

        :param data: data to save into the log file
        :return:
        """
        path = common.PATH + "power_consumption.log"
        with open(path, 'w+') as f:
            json.dump(data, f, indent=2)

    @staticmethod
    def create_turn_on_date_log_file(data):
        """ Create a log file which contains the time when each host has
        been turned on

        :param data: data to save into the log file
        :return:
        """
        path = common.PATH + "turn_on_date.log"
        with open(path, 'w+') as f:
            json.dump(data, f, indent=2)

    @staticmethod
    def create_turn_off_date_log_file(data):
        """ Create a log file which contains the time when each host has
        been turned off

        :param data: data to save into the log file
        :return:
        """
        path = common.PATH + "turn_off_date.log"
        with open(path, 'w+') as f:
            json.dump(data, f, indent=2)
