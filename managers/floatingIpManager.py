# @package floatingIpManager
# Documentation of the Floating IP Manager package
#

import my_logger

logger = my_logger.get_logger('FloatingIpManager')


class FloatingIpManager:

    def __init__(self, client):
        """ Constructor of FloatingIpManager

        :param client: Nova client given by the ClientManager
        :return:
        """

        # local instance of the Client
        self.client = client

    def add_floating_ip_to_instance(self, instance):
        """ Give a floating IP to the instance in parameter

        :param instance: OpenStack instance that needs a floating IP
        :return:
        """
        new_floating_ip = self.create_floating_ip()
        instance.add_floating_ip(new_floating_ip)

    def create_floating_ip(self):
        """ Return a newly created floating IP

        :return:
        """
        return self.client.floating_ips.create("public").ip