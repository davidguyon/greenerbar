# @package imageManager
# Documentation of the Image Manager package
#

import my_logger

logger = my_logger.get_logger('ImageManager')


class ImageManager:

    def __init__(self, client):
        """ Constructor of ImageManager

        :param client: Nova client given by the ClientManager
        :return:
        """

        # local instance of the Client
        self.client = client

    def get_all_images(self):
        """ Return the list of images

        :return:
        """
        return self.client.images.list()

    def get_image_by_id(self, image_id):
        """ Return image details of the image matching the id in parameter

        :param image_id: id of an existing OpenStack image
        :return: image object or None
        """
        try:
            image = self.client.images.get(images=image_id)
            return image
        except Exception as e:
            logger.error(e)
            return None

    def get_image_by_name(self, image_name):
        """ Get image details of the image matching the name in parameter

        :param image_name: String equal to the image name
        :return: image object matching the name or None
        """
        images = self.get_all_images()
        for image in images:
            if image.name == image_name:
                return image
        logger.warning("No image found for the name: " + image_name)
        return None

    @staticmethod
    def show_image_info(image):
        """ Print details of the image in parameter

        :param image: OpenStack image to show
        :return:
        """
        print("Image: " + image.name)
        print("\tStatus: " + str(image.status))
        print("\tmin memory: " + str(image.minRam))
        print("\tmin disk: " + str(image.minDisk))
