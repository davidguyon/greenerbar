# @package jobManager
# Documentation of the Job Manager package
#

import my_logger

logger = my_logger.get_logger('JobManager')


class JobManager:

    def __init__(self, name):
        """ Constructor of JobManager

        :param name: name of the workflow to handle
        :return:
        """

        # Workflow's name
        self.name = name
        # List of jobs
        self.jobs_list = []
        # CPU util data of master instance
        self.cpu_util = []
        # Zone on which the master is running on
        self.__master_zone = ''
        # id of the instance on which the master is running on
        self.__master_instance_id = None

    def add_job(self, name, json):
        """ Append a new created job to the intern list of jobs

        :param name: name of the job
        :param json: Json object with the data specified in job-sample.yaml
        :return:
        """
        if 'src' not in json:
            logger.error("src missing in "+name+" job definition")
        if 'cmd' not in json:
            logger.error("cmd missing in "+name+" job definition")
        if 'input_dir' not in json:
            logger.error("input_dir missing in "+name+" job definition")
        if 'input_type' not in json:
            logger.error("input_type missing in "+name+" job definition")
        if 'output_dir' not in json and 'output_type' not in json:
            logger.info("No upload folder specified for the output files "
                        "of the job "+name+" (optional)")
        elif 'output_dir' not in json:
            logger.error("output_dir missing in "+name+" job definition")
        elif 'output_type' not in json:
            logger.error("output_type missing in "+name+" job definition")

        src = json.get('src')
        if src is None:
            src = ''
        cmd = json.get('cmd')
        if cmd is None:
            cmd = ''
        input_dir = json.get('input_dir')
        if input_dir is None:
            input_dir = ''
        input_type = json.get('input_type')
        if input_type is None:
            input_type = ''
        output_dir = json.get('output_dir')
        if output_dir is None:
            output_dir = ''
        output_type = json.get('output_type')
        if output_type is None:
            output_type = ''
        post_run = json.get('post_run')

        job = Job(name,
                  src, cmd,
                  input_dir, input_type,
                  output_dir, output_type,
                  json.get('cpu'),
                  json.get('ram'),
                  json.get('disk'),
                  json.get('nb_instances'),
                  post_run)
        self.jobs_list.append(job)

    def get_jobs(self):
        """ Return the list of jobs

        :return: list of jobs
        """
        return self.jobs_list

    def get_last_job(self):
        """ Return the last created job

        :return: last created job
        """
        return self.jobs_list[-1]

    def get_name(self):
        """ Return the workflow's name

        :return: workflow's name
        """
        return self.name

    def get_master_cpu_util(self):
        """ Return the cpu util data of the master instance

        :return: cpu util data of the master instance
        """
        return self.cpu_util

    def save_master_cpu_util(self, instance_id, cpu_util_data):
        """ Save the cpu util data of the master instance

        :param instance_id: master instance's id
        :param cpu_util_data: cpu util data of the master's instance
        :return:
        """
        self.cpu_util.append({str(instance_id): cpu_util_data})

    def save_master_zone(self, zone):
        """ Save the master's zone

        :param zone: zone on which the master is running on
        :return:
        """
        self.__master_zone = zone

    def get_master_zone(self):
        """ Return the zone on which the master instance is running

        :return: zone on which the master instance is running
        """
        return self.__master_zone

    def save_master_instance_id(self, instance_id):
        """ Save the master instance's id

        :param instance_id: master instance's id
        :return:
        """
        self.__master_instance_id = instance_id

    def get_master_instance_id(self):
        """ Return the id of the instance on which the master is running

        :return: master instance's id
        """
        return self.__master_instance_id


class Job:

    def __init__(self, name, src, cmd, input_dir, input_type, output_dir,
                 output_type, cpu, ram, disk, nb_instances, post_run=None):
        """ Constructor of Job

        :param name: name of the job
        :param src: source file to execute
        :param cmd: command to execute on the job
        :param input_dir: path to the input directory
        :param input_type: prefix of the files to use in the input directory
        :param output_dir: path the the output directory
        :param output_type: prefix of the output files
        :param cpu: number of CPU needed for the job
        :param ram: amount of RAM needed for the job
        :param disk: amount of disk needed for the job
        :param nb_instances: number of instances for this job
        :param post_run: bash script that will be run on the controller
                        after the run (optional)
        :return:
        """

        # Job's name
        self.__name = name
        # Source file to execute
        self.__src = src
        # Command for the job
        self.__cmd = cmd
        # Path to the input directory
        self.__input_dir = input_dir
        # Prefix of the files to use in the input directory
        self.__input_type = input_type
        # Path to the output directory
        self.__output_dir = output_dir
        # Prefix of the output files
        self.__output_type = output_type
        # Needed number of CPU
        self.__cpu = cpu
        # Needed amount of RAM in MB
        self.__ram = ram
        # Needed amount of disk in GB
        self.__disk = disk
        # Needed number of instances
        self.__nb_instances = 1 if nb_instances is None else nb_instances
        # Bash script that will be run on controller after the execution
        # of the job (if parameter is missing, equal to None)
        self.__post_run = post_run
        # Instances for the job
        self.__instances = []
        # Zone of each instance
        self.__zone_for_instance = []
        # CPU util data for each instance
        self.__cpu_util = []

    def get_name(self):
        """ Return the name of the job

        :return: job's name
        """
        return self.__name

    def get_source(self):
        """ Return the source file to execute*

        :return: source file to execute
        """
        return self.__src

    def get_command(self):
        """ Return the command to execute for this job

        :return: command to execute
        """
        return self.__cmd

    def get_input_dir(self):
        """ Return the path pointing to the input directory

        :return: path pointing to the input directory
        """
        return self.__input_dir

    def get_input_type(self):
        """ Return the prefix of the files to use in the input directory

        :return: prefix of the input files to use
        """
        return self.__input_type

    def get_output_dir(self):
        """ Return the path pointing to the output directory

        :return: path pointing to the output directory
        """
        return self.__output_dir

    def get_output_type(self):
        """ Return the prefix of the output files to upload

        :return: prefix of the output files
        """
        return self.__output_type

    def get_post_run(self):
        """ Return the bash script to run after the job execution

        :return: bash script to run after the job execution
        """
        return self.__post_run

    def get_cpu(self):
        """ Return the required number of CPU for the job

        :return: required number of CPU for the job
        """
        return self.__cpu

    def get_ram(self):
        """ Return the required amount of RAM for the job

        :return: required amount of RAM for the job
        """
        return self.__ram

    def get_disk(self):
        """ Return the required amount of disk for the job

        :return: required amount of disk for the job
        """
        return self.__disk

    def get_nb_instances(self):
        """ Return the number of instances needed for the job

        :return: number of instances needed for the job
        """
        return self.__nb_instances

    def get_instances(self):
        """ Return the list of instances created for the current job

        :return: list of instances'
        """
        return self.__instances

    def add_instance(self, instance):
        """ Add the instance in parameter to the list of instances

        :param instance: instance for this job
        :return:
        """
        self.__instances.append(instance)
        if len(self.__instances) > self.__nb_instances:
            logger.warning("More instances than the number "
                           "of needed instances")

    def save_zone_for_instance(self, instance_id, zone, name):
        """ Save the zone on which the instance in parameter is running

        :param instance_id: id of the instance running on the given zone
        :param zone: zone on which the given instance is running on
        :param name: name of the job or "master"
        :return:
        """
        self.__zone_for_instance.append({
            "instance_id": str(instance_id),
            "zone": zone,
            "name": name
        })

    def get_zone_for_instance(self):
        """ Return the table with the zone on which each instance ran

        :return: table with the zone on which each instance ran
        """
        return self.__zone_for_instance

    def get_zone_of_instance(self, instance_id):
        """ Return the zone used by the instance in parameter, None otherwise

        :param instance_id: id of an instance
        :return: zone used by the instance in parameter of None
        """
        for zone in self.__zone_for_instance:
            if zone['instance_id'] == str(instance_id):
                return zone['zone']
        return None

    def get_all_cpu_util(self):
        """ Return all cpu util data

        :return: all cpu util data
        """
        return self.__cpu_util

    def get_cpu_util(self, instance_id):
        """ Return the cpu util data of the instance given in parameter,
        otherwise None

        :param instance_id: id of an instance
        :return: cpu util of the instance in parameter, otherwise None
        """
        for instance in self.__cpu_util:
            if instance["instance_id"] == str(instance_id):
                return instance["cpu_util"]
        return None

    def save_cpu_util(self, instance_id, cpu_util_data):
        """ Save the cpu util data of the instance in parameter

        :param instance_id: id of the instance
        :param cpu_util_data: cpu util data of the instance in parameter
        :return:
        """
        self.__cpu_util.append({
            "instance_id": str(instance_id),
            "cpu_util": cpu_util_data})
