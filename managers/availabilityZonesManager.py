# @package availabilityZoneManager
# Documentation of the Availability Zones Manager package
#

import my_logger

logger = my_logger.get_logger('AvailabilityZonesManager')


class AvailabilityZonesManager:

    def __init__(self, client):
        """ Constructor of AvailabilityZonesManager

        :param client: Nova client given by the ClientManager
        :return:
        """

        # local instance of the Client
        self.client = client

    def get_all_availability_zones(self):
        """ Return the list of availability zones

        :return: list of availability zones
        """
        return self.client.availability_zones.list()
