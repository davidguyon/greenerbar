# @package ceilometerManager
# Documentation of the Ceilometer Manager package
#

from ceilometerclient.client import get_client as CeilometerClient

import my_logger

logger = my_logger.get_logger('CeilometerManager')


class CeilometerManager:

    def __init__(self, client_manager):
        """ Constructor of CeilometerManager

        :param client_manager: ClientManager instance (Nova client)
        :return:
        """

        # OpenStack API version
        self.VERSION = client_manager.VERSION
        # OpenStack username used to connect to the API
        self.USERNAME = client_manager.USERNAME
        # OpenStack password used to connect to the API
        self.PASSWORD = client_manager.PASSWORD
        # OpenStack API authentication URL
        self.AUTH_URL = client_manager.AUTH_URL
        # OpenStack project ID
        self.PROJECT_ID = client_manager.PROJECT_ID

        # local instance of the Ceilometer client
        self.client = CeilometerClient(self.VERSION,
                                       os_username=self.USERNAME,
                                       os_password=self.PASSWORD,
                                       os_tenant_name=self.PROJECT_ID,
                                       os_auth_url=self.AUTH_URL)

    def get_cpu_util(self, resource_id):
        """ Return all cpu_util data for the instance specified in parameter

        :param resource_id: id of the instance from which to get data
        :return: cpu_util data of the instance in parameter
        """
        values = []
        query = [dict(field='resource_id', op='eq', value=resource_id)]
        resp = self.client.samples.list(meter_name='cpu_util',
                                        limit=None, q=query)

        for entry in resp:
            values.append({
                'timestamp': entry.timestamp,
                'value': entry.counter_volume
            })

        return values
