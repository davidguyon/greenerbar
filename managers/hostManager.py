# @package hostManager
# Documentation of the Host Manager package
#

import sys
import time
import threading

import my_logger
from managers.aggregatesManager import AggregatesManager

logger = my_logger.get_logger('HostManager')


class HostManager:

    def __init__(self, client):
        """ Constructor of HostManager

        :param client: Nova client given by the ClientManager
        :return:
        """

        # local instance of the Client
        self.client = client

        # local dictionary of host (empty)
        self.host_dict = []

        # dict of date when each host has been turned on
        self.dict_host_on = {}

        # dict of date when each host has been turned off
        self.dict_host_off = {}

        # Lock to secure the turning on and off of hosts
        self.lock = threading.Lock()

    def get_host(self, hostname):
        """ Return the details of the host matching the name given in
        parameter

        :param hostname: name of the host from which to get details
        :return: details of host matching the name in parameter
        """
        try:
            return self.client.hosts.get(host=hostname)
        except Exception as e:
            logger.error(e)
            return None

    def get_hosts(self, zone="nova"):
        """ Return the list of hosts present in the zone given in parameter
        If zone is not given, it returns the list of hosts present in the
        "nova" zone

        :param zone: zone from where to get the host (default is "nova")
        :return: list of hosts in specified zone
        """
        return self.client.hosts.list(zone=zone)

    def get_detailed_hosts(self, zone="nova"):
        """ Return the list of hosts present in the zone given in parameter
        If zone is not given, it returns the list of hosts present in the
        "nova" zone
        Each host in the list contains all available details

        :param zone: zone from where to get the hosts details
        :return: list of detailed host in specified zone
        """
        all_hosts = self.get_hosts(zone)
        all_hosts_detailed = []
        for h in all_hosts:
            all_hosts_detailed.append(self.get_host(h))
        return all_hosts_detailed

    def initiate_host_dict(self, aggregates_manager):
        """ Initialize the hosts dictionaries

        :param aggregates_manager: AggregatesManager to get the list of zones
        :return:
        """
        for zone in aggregates_manager.get_all_aggregates():  # for each zone
            hosts = self.get_hosts(zone.name)
            if len(hosts) == 0:
                logger.warning("zone " + zone.name + " is empty")
            elif len(hosts) > 1:
                logger.warning("zone " + zone.name + " has more than 1 host")
            else:
                host_name = hosts[0].host_name
                # keeps only "taurus-X"
                hostname_clean = '-'.join(host_name.split('-')[:2])
                self.dict_host_on[hostname_clean] = []
                self.dict_host_off[hostname_clean] = []
                zone_name = hosts[0].zone
                host_detailed = self.get_host(host_name)
                self.host_dict.append({
                    'host': host_name,
                    'zone': zone_name,
                    'cpu': self.get_cpu_total(host_detailed),
                    'ram': self.get_ram_total(host_detailed),
                    'disk': self.get_disk_total(host_detailed),
                    'state': 'on'
                })

    def turn_on(self, hostname):
        """ Power on the host matching the name in parameter

        :param hostname: name of the host to turn on
        :return:
        """
        for host in self.host_dict:
            if hostname == host['host']:
                if host['state'] == 'on':
                    logger.warning('host ' + hostname + ' is already on')
                else:
                    # Avoid failing to find a turned on host
                    # while one is turning on
                    self.lock.acquire()

                    # keep only "taurus-7"
                    hostname_clean = '-'.join(hostname.split('-')[:2])
                    self.dict_host_on[hostname_clean].append(int(time.time()))
                    host['state'] = 'on'
                    logger.info('host ' + hostname + ' is turning on')
                    logger.info('Will wait for 2 minutes and 30 seconds '
                                'to simulate turning on ' + hostname)
                    # sleep for 2 min 30 to simulate a host turning on
                    time.sleep(150)

                    self.lock.release()

    def turn_off(self, hostname):
        """ Power off the host in parameter

        :param hostname: name of the host to turn off
        :return:
        """
        for host in self.host_dict:
            if hostname == host['host']:
                if host['state'] == 'off':
                    logger.warning('host ' + hostname + ' is already off')
                else:
                    # keep only "taurus-7"
                    hostname_clean = '-'.join(hostname.split('-')[:2])
                    self.dict_host_off[hostname_clean].append(int(time.time()))
                    host['state'] = 'off'
                    logger.info('host ' + hostname + ' is turning off')

    def is_on(self, hostname):
        """ Return True if the host in parameter is turned on

        :param hostname: name of the host to check the state
        :return: True if host is turned on, False otherwise
        """
        for host in self.host_dict:
            if hostname == host['host']:
                if host['state'] == 'on':
                    return True
                else:
                    return False

    def is_off(self, hostname):
        """ Return True if the host in parameter is turned off

        :param hostname: name of the host to check the state
        :return: True if the host is turned off, False otherwise
        """
        for host in self.host_dict:
            if hostname == host['host']:
                if host['state'] == 'off':
                    return True
                else:
                    return False

    @staticmethod
    def get_hostname(host):
        """ Return the name of the host in parameter

        :param host: detailed host
        :return: name of the host
        """
        return host[0].host

    @staticmethod
    def get_cpu_total(host):
        """ Return the total number of CPU on the host in parameter

        :param host: detailed host
        :return: number of CPU on host
        """
        return host[0].cpu

    @staticmethod
    def get_cpu_used_now(host):
        """ Return the number of CPU used now on the host in parameter

        :param host: detailed host
        :return: number of CPU used now on host
        """
        return host[1].cpu

    @staticmethod
    def get_cpu_used_max(host):
        """ Return the number of CPU used max on the host in parameter

        :param host: detailed host
        :return: number of CPU used max on host
        """
        return host[2].cpu

    @staticmethod
    def get_ram_total(host):
        """ Return the total amount of RAM on the host in parameter

        :param host: detailed host
        :return: total amount of RAM on host
        """
        return host[0].memory_mb

    @staticmethod
    def get_ram_used_now(host):
        """ Return the amount of RAM used now on the host in parameter

        :param host: detailed host
        :return: amount of RAM used now on host
        """
        return host[1].memory_mb

    @staticmethod
    def get_ram_used_max(host):
        """ Return the amount of RAM used max on the host in parameter

        :param host: detailed host
        :return: amount of RAM used max on host
        """
        return host[2].memory_mb

    @staticmethod
    def get_disk_total(host):
        """ Return the total amount of disk on the host in parameter

        :param host: detailed host
        :return: total amount of disk on host
        """
        return host[0].disk_gb

    @staticmethod
    def get_disk_used_now(host):
        """ Return the amount of disk used now on the host in parameter

        :param host: detailed host
        :return: amount of disk used now on host
        """
        return host[1].disk_gb

    @staticmethod
    def get_disk_used_max(host):
        """ Return the amount of disk used max on the host in parameter

        :param host: detailed host
        :return: amount of disk used max on host
        """
        return host[2].disk_gb

    @staticmethod
    def available_cpu(host):
        """ Return the number of available CPU on the host in parameter

        :param host: detailed host
        :return: number of available CPU on host
        """
        cpu_total = HostManager.get_cpu_total(host)
        cpu_used_now = HostManager.get_cpu_used_now(host)
        return cpu_total - cpu_used_now

    @staticmethod
    def available_ram(host):
        """ Return the amount of available RAM on the host in parameter

        :param host: detailed host
        :return: amount of available RAM on host
        """
        ram_total = HostManager.get_ram_total(host)
        ram_used_now = HostManager.get_ram_used_now(host)
        return ram_total - ram_used_now

    @staticmethod
    def available_disk(host):
        """ Return the amount of available disk on the host in parameter

        :param host: detailed host
        :return: amount of available disk on host
        """
        disk_total = HostManager.get_disk_total(host)
        disk_used_now = HostManager.get_disk_used_now(host)
        return disk_total - disk_used_now

    @staticmethod
    def show_host_info(host):
        """ Print the details of the host in parameter

        :param host: detailed host
        :return:
        """
        print("Host: " + HostManager.get_hostname(host))
        for data in host:
            print(str(data.project))
            print("\tCPU: " + str(data.cpu))
            print("\tMemory: " + str(data.memory_mb))
            print("\tHDD: " + str(data.disk_gb))

    def which_host_for_flavor(self, flavor):
        """ Return the best suitable host for the future instance that will
        be created with the flavor given in parameter

        :param flavor: existing OpenStack flavor
        :return:
        """
        value_to_return = None

        logger.debug("which_host_for_flavor")

        aggregates_manager = AggregatesManager(self.client)
        aggregates = aggregates_manager.get_all_aggregates()

        suitable_hosts = []

        for aggregate in aggregates:
            zone_name = aggregate.name
            hosts = self.get_hosts(zone_name)

            if len(hosts) > 1:
                logger.warning("There are too much hosts on the "
                               "zone named '" + zone_name + "'")
                logger.warning("Ignoring other hosts")
            elif len(hosts) == 0:
                logger.error("The zone '" + zone_name + "' is empty")
                sys.exit("Failed at which_host_for_flavor")

            # Ignore this host if turned off
            if self.is_off(hosts[0].host_name):
                continue

            # Get CPU/RAM/disk details of the host
            host = self.get_host(hosts[0].host_name)

            # Enough CPU?
            #print("Need: " + str(flavor.vcpus))
            #print("Available: " + str(HostManager.available_cpu(host)))
            if self.available_cpu(host) < flavor.vcpus:
                continue

            # Enough RAM?
            #print("Need: " + str(flavor.ram))
            #print("Available: " + str(HostManager.available_ram(host)))
            if self.available_ram(host) < flavor.ram:
                continue

            # Enough disk space?
            #print("Need: " + str(flavor.disk))
            #print("Available: " + str(HostManager.available_disk(host)))
            if self.available_disk(host) < flavor.disk:
                continue

            # Save the zone of the current host
            host.append(hosts[0].zone)

            suitable_hosts.append(host)

        if len(suitable_hosts) == 0:
            logger.info('Searching for turned off hosts to turn on')

            # List turned off hosts
            turned_off = []
            for host in self.host_dict:
                if self.is_off(host['host']):
                    turned_off.append(host)

            def check_enough_resource(_flavor, _host):
                if _host['cpu'] < _flavor.vcpus:
                    return False
                if _host['ram'] < _flavor.ram:
                    return False
                if _host['disk'] < _flavor.disk:
                    return False
                return True

            # Did not find any turned off host
            if len(turned_off) == 0:
                logger.warning('Unable to find a suitable host')
                logger.warning('Reason: turned_off empty')
                value_to_return = None

            # Found only one host
            elif len(turned_off) == 1:
                host_off = turned_off[0]
                if check_enough_resource(flavor, host_off):
                    self.turn_on(host_off['host'])
                    host = self.get_host(host_off['host'])
                    host.append(host_off['zone'])
                    value_to_return = host
                else:
                    logger.warning('Unable to find a suitable host')
                    logger.warning('Reason: ' + host_off['host'] + 'available'
                                   ' but does not have enough resource')
                    value_to_return = None
            
            # Found multiple turned off hosts
            else:
                logger.info('Found multiple turned off hosts')
                value_to_return = None
                for h in turned_off:
                    if check_enough_resource(flavor, h):
                        self.turn_on(h['host'])
                        host = self.get_host(h['host'])
                        host.append(h['zone'])
                        value_to_return = host
                        break  # leave the for loop when a host has been found
                if value_to_return is None:
                    logger.warning('Unable to find a suitable host')
                    logger.warning('Reason: none of the turned off hosts '
                                   'had enough resources')

        elif len(suitable_hosts) == 1:
            value_to_return = suitable_hosts[0]
            logger.info('Found 1 suitable host: ' + str(value_to_return))

        else:
            logger.info('Found many suitable hosts')
            logger.ionfo('Searching for the best suitable one')

            # detailed_hosts variable
            # List of dict containing:
            # - the current host
            # - its delta value with the current resource need
            # - the number of time where the delta value for a
            #   specific resource (CPU, RAM or disk) meets zero
            detailed_hosts = []

            min_delta_value = None

            # Calculate delta_value and nb_zero for each suitable host
            for host in suitable_hosts:
                delta_value = 0
                nb_zero = 0

                def delta(available, needed, ratio):
                    return (available - needed)/ratio

                # CPU
                delta_cpu = delta(
                    HostManager.available_cpu(host),
                    flavor.vcpus,
                    1)
                if delta_cpu == 0:
                    nb_zero += 1
                delta_value += delta_cpu

                # RAM
                delta_ram = delta(
                    HostManager.available_ram(host),
                    flavor.ram,
                    512)
                if delta_ram == 0:
                    nb_zero += 1
                delta_value += delta_ram

                # disk
                delta_disk = delta(
                    HostManager.available_disk(host),
                    flavor.disk,
                    5)
                if delta_disk == 0:
                    nb_zero += 1
                delta_value += delta_disk

                print("Zone: " + host[len(host)-1])
                print("  delta_value=" + str(delta_value))
                print("  nb_zero=" + str(nb_zero))

                # Save the detailed_host in the detailed_hosts list
                detailed_host = {
                    'host': host,
                    'delta_value': delta_value,
                    'nb_zero': nb_zero
                }
                detailed_hosts.append(detailed_host)

                # If delta_value is the min hit so far
                # save it to min_delta_value
                if (min_delta_value is None) or \
                        (delta_value < min_delta_value):
                    min_delta_value = delta_value

            # print "Before min delta"
            # for foo in detailed_hosts: print foo

            # detailed_hosts_min_delta_value variable
            # List of detailed_host that have their
            # delta_value equal to the min_delta_value
            detailed_hosts_min_delta_value = \
                [h for h in detailed_hosts
                 if h['delta_value'] == min_delta_value]

            # print "After min delta"
            # for foo in detailed_hosts_min_delta_value: print foo

            if len(detailed_hosts_min_delta_value) == 0:
                logger.error("detailed_hosts_min_delta_value "
                             "should not be equal to zero")

            elif len(detailed_hosts_min_delta_value) == 1:
                logger.info("Only 1 host has the min delta value")
                value_to_return = detailed_hosts_min_delta_value[0]['host']

            else:
                logger.info("Many hosts have the min delta value")
                logger.info("Searching for the max delta to zero")
                detailed_host_best_nb_zero = None

                for detailed_host in detailed_hosts_min_delta_value:

                    # If detailed_host_best_nb_zero empty
                    # save the current detailed_host
                    if detailed_host_best_nb_zero is None:
                        detailed_host_best_nb_zero = detailed_host

                    # Update detailed_host_best_nb_zero
                    # if the current is better
                    elif detailed_host_best_nb_zero['nb_zero'] < \
                            detailed_host['nb_zero']:
                        detailed_host_best_nb_zero = detailed_host

                host = detailed_host_best_nb_zero['host']
                delta_value = detailed_host_best_nb_zero['delta_value']
                nb_zero = detailed_host_best_nb_zero['nb_zero']

                logger.info(host[0].host + ", delta:" + str(delta_value) +
                            ", nb zero:" + str(nb_zero))

                if nb_zero > 3:
                    logger.warning("nb_zero should not be more than 3")

                elif nb_zero == 3:
                    logger.info("nb_zero is equal to 3, perfect!")

                value_to_return = host

        return value_to_return

    def turn_off_empty_hosts(self):
        """ Detect hosts that are empty and turn them off

        :return:
        """
        
        # Avoid turning off a host during an instance creation 
        self.lock.acquire()

        # Wait few seconds for OpenStack to update its variables
        time.sleep(10)

        turned_on = []
        for host in self.host_dict:
            if self.is_on(host['host']):
                turned_on.append(host)

        for host in turned_on:
            detailed_host = self.get_host(host['host'])

            # Ignore hosts that are not empty
            if HostManager.get_cpu_used_now(detailed_host) > 0:
                continue
            # 512MB of RAM is allocated to the nova compute service
            if HostManager.get_ram_used_now(detailed_host) > 512:
                continue
            if HostManager.get_disk_used_now(detailed_host) > 0:
                continue

            self.turn_off(host['host']) 

        self.lock.release()
