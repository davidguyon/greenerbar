#!/usr/bin/python

import os
import time
from threading import Thread

import common
import my_logger
from managers.aggregatesManager import AggregatesManager
from managers.ceilometerManager import CeilometerManager
from managers.clientManager import ClientManager
from managers.powerManager import PowerManager
from managers.hostManager import HostManager
from managers.logsManager import LogsManager
from workflow import Workflow

logger = my_logger.get_logger('Main')


# Get the Nova API client
client_manager = ClientManager()
nova_client = client_manager.client

aggregates_manager = AggregatesManager(nova_client)
host_manager = HostManager(nova_client)

# Check if the zones are already created by looking at
# the hosts remaining on the "nova" zone
aggregates_manager.initiate_zones(host_manager)

host_manager.initiate_host_dict(aggregates_manager)

ceilometer_manager = CeilometerManager(client_manager)

threads = []

# Grid5000 job ID of this OpenStack deployment
yaml_file = Workflow.load_yaml_file("/root/job_status.yaml")
job_id = list(yaml_file.keys())[0]

# Folder's name where to copy execution logs
folder_name = 'simulation_palmtree'

print("##############################")
print("# GreenerBar by David Guyon  #")
print("# started in March 2015      #")
print("# last update: February 2016 #")
print("##############################")

enough_workflow = False
index = 0
while not enough_workflow:
    print("> Add a workflow")
    #jobs_definition = input("  Name of the workflow: ")
    jobs_definition = "palmtree/palmtree.yaml"
    print("  Green value")
    print("  -1: energy saving")
    print("   0: normal")
    print("   1: performance")
    green_value = input("  value: ")

    workflow = Workflow(jobs_definition, green_value, ceilometer_manager,
                        host_manager, 4242 + (index * 100), folder_name)
    index += 1
    thread = Thread(target=workflow.start)
    threads.append(thread)

    another_workflow = input("> Add another workflow? [y/N]")
    if another_workflow != 'y':
        enough_workflow = True

# Save the starting time
start_time = time.time()

# Start all threads and wait for all of them to terminate
# for each workflow: waits index * 20 seconds before starting
# index = 0
for thread in threads:
    # time.sleep(index*20)
    # index += 1
    thread.start()
for thread in threads:
    thread.join()

# Save the end time and calculate the total time
end_time = time.time()
total_time = end_time-start_time

logger.info("The program executed in " + str(total_time) + " seconds")
print("Start time: " + str(start_time))
print("End time: " + str(end_time))

seconds = 150
logger.info("Wait for " + str(seconds) + "sec for the PowerManager "
                                         "to get all data")
time.sleep(seconds)
power_manager = PowerManager()
power_consumption_data = power_manager.get_power_consumption(job_id,
                                                             start_time)

logs_manager = LogsManager()
logs_manager.create_power_consumption_log_file(power_consumption_data)
logs_manager.create_turn_on_date_log_file(host_manager.dict_host_on)
logs_manager.create_turn_off_date_log_file(host_manager.dict_host_off)

destination = common.PATH + 'graphs_gen/' + folder_name + '/'

if not os.path.exists(destination):
    os.makedirs(destination)

os.system('mv ' + common.PATH + 'power_consumption.log ' + destination)
os.system('mv ' + common.PATH + 'turn_on_date.log ' + destination)
os.system('mv ' + common.PATH + 'turn_off_date.log ' + destination)
os.system('cp ' + common.PATH + 'turning_on_data.log ' + destination)
