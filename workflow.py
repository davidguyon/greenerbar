# @package workflow
# Documentation of the Workflow package
#

import os
import sys
import time
import socket
import subprocess
import ruamel.yaml as yaml
from threading import Thread

import common
import my_logger
from managers.postInstallScriptsManager import PostInstallScriptsManager
from managers.securityGroupsManager import SecurityGroupsManager
from managers.floatingIpManager import FloatingIpManager
from managers.keypairsManager import KeypairsManager
from managers.instanceManager import InstanceManager
from managers.flavorManager import FlavorManager
from managers.imageManager import ImageManager
from managers.jobManager import JobManager
from managers.logsManager import LogsManager

logger = my_logger.get_logger("Workflow")


class Workflow:

    def __init__(self, name, green, ceilometer_manager, host_manager, port,
                 folder_name):
        """ Constructor of Workflow

        :param name: Name of the workflow to load
        :param green: Amount of 'Green' for the run
        :param ceilometer_manager: CeilometerManager instance
        :param host_manager: HostManager instance
        :param port: Port that will be used for the callback system
        :param folder_name: Name of the folder where to copy execution logs
        :return:
        """

        # Workflow's name
        self.filename = name

        # Name of the file without the full path and the prefix
        # ex: /foo/bar.yaml -> ['','foo','bar.yaml'] -> 'bar.yaml' -> 'bar'
        self.name = name.split('/')[-1][:-5]
        self.jobManager = JobManager(name)

        # Port used by the socket while waiting for instances to be ready
        self.port = port

        # Name of the folder where to copy the execution logs
        self.folder_name = folder_name

        # Green value for the flavor selection
        self.green_value = int(green)

        # Get the Nova API client from the HostManager
        self.client = host_manager.client

        # Get all needed managers
        self.securityGroupsManager = SecurityGroupsManager(self.client)
        self.postInstallScriptsManager = PostInstallScriptsManager()
        self.floatingIpManager = FloatingIpManager(self.client)
        self.instanceManager = InstanceManager(self.client)
        self.keypairsManager = KeypairsManager(self.client)
        self.flavorManager = FlavorManager(self.client)
        self.imageManager = ImageManager(self.client)
        self.ceilometerManager = ceilometer_manager
        self.hostManager = host_manager

        # Master instance
        self.master_instance = None

        # Load workflow description file and create Job(s) accordingly
        loaded_file = self.load_yaml_file(common.WORKFLOWS_PATH +
                                          self.filename)
        for key, values in loaded_file.items():
            self.jobManager.add_job(key, values)

        logger.info("Workflow " + self.name + " ready, waiting to start")

    def start(self):
        """ Run the workflow by calling "prepare" and "execute" on each job

        :return:
        """

        # Create the master instance which will be used by FRIEDA
        self._create_master_instance()
        self._wait_for_master()

        # Give to the master instance a floating IP address
        floating_ip = self.floatingIpManager.create_floating_ip()
        logger.info("Adding floating ip " + str(floating_ip) + " to instance...")
        self.master_instance.add_floating_ip(floating_ip)

        jobs = self.jobManager.get_jobs()

        previous_job = None
        for job in jobs:
            self.prepare_job(job)
            if previous_job is not None:
                # input("Press Enter to continue...")
                self._delete_instances_of_job(previous_job)
            self.execute_job(job)
            previous_job = job

        input("Press Enter to continue...")
        self._delete_instances_of_job(previous_job)

        logger.info(self.name + ": save logs and delete master instance")

        # Save CPU util data
        logs_manager = LogsManager()
        master_id = self.master_instance.id
        cpu_util_data = self.ceilometerManager.get_cpu_util(master_id)
        self.jobManager.save_master_cpu_util(master_id, cpu_util_data)
        zone = self.jobManager.get_master_zone()
        logs_manager.create_cpu_util_log_file(self.name, "master", zone,
                                              master_id, cpu_util_data)

        # Delete master instance
        self.instanceManager.delete_instance(master_id)
        time.sleep(1)

        self.hostManager.turn_off_empty_hosts()

        logger.info(self.name + ": review of zones used by hosts")
        for job in jobs:
            print(job.get_name() + ": " + job.get_zone_for_instance())

        # Move logs for graph generation
        self._move_logs_to_graphs_gen_folder(self.folder_name)
        self._clean_execution()

    def prepare_job(self, job):
        """ Prepare everything for the execution of the job in parameter

        :param job: Job to prepare
        :return:
        """

        def _prepare_instance_then_frieda(_job):
            self._prepare_instances(_job)
            self._prepare_frieda(_job)

        p1 = Thread(target=self._wait_for_instances, args=(job,))
        p2 = Thread(target=_prepare_instance_then_frieda, args=(job,))
        p1.start()
        p2.start()
        p1.join()
        p2.join()

        self._prepare_floating_ips(job)

        logger.info(self.name + ": " + job.get_name() + " ready for execution")

    def execute_job(self, job):
        """ Execute the job in parameter

        :param job: Job to execute
        :return:
        """

        job_name = job.get_name()
        logger.info(self.name + ": " + job_name + " starts its execution")

        self._execute_frieda(job)

        logger.info(self.name + ": " + job_name + " execution terminated")

    def print_workflow(self):
        """ Print the workflow details

        :return:
        """
        logger.debug("print_workflow")
        print("Workflow: " + self.jobManager.get_name())
        jobs = self.jobManager.get_jobs()
        for job in jobs:
            print("-", job.get_name())
            print("    cmd: " + job.get_command())
            print("    cpu: " + job.get_cpu())
            print("    ram: " + job.get_ram())
            print("    disk:" + job.get_disk())

    @staticmethod
    def load_yaml_file(name):
        """ Load the YAML file at the path given in parameter

        :param name: Name of the YAML file to load
        :return: instance of the YAML file
        """
        try:
            stream = open(name, 'r')
            return yaml.load(stream, Loader=yaml.RoundTripLoader)
        except Exception as e:
            logger.error(e)
            sys.exit("Failed at _load_yaml_file")

    def _prepare_instances(self, job):
        """ Create instance for each task with the best suitable flavor

        :param job: job for which to create instances
        :return:
        """

        post_install_script = None
        image = None

        if self.name == "montage":
            # Select the ubuntu-14.04-montage image
            # todo generate suitable image
            image = self.imageManager\
                .get_image_by_name("Debian Jessie 64-bit")

            # prepare the montage post install script depending on the step
            # if stepB, it adds the bash line to do the scp from stepA to stepB
            # of *.fits files
            if job.get_name() == "stepB":
                previous_job = self.jobManager.get_jobs()[1]
                workers = previous_job.get_instance()
                ips = []
                for worker in workers:
                    instance = self.instanceManager.get_instance(worker.id)
                    ip = instance.networks['net-jdoe'][0]
                    logger.info(self.name + ": IP of worker of job " +
                                job.get_name() + " is " + str(ip))
                    ips.append(ip)
                post_install_script = self.postInstallScriptsManager\
                    .get_montage_script(self.port, ips=ips)
            else:
                post_install_script = self.postInstallScriptsManager\
                    .get_montage_script(self.port)

        elif self.name == "palmtree":
            # Select the debian-palmtree image
            image = self.imageManager.get_image_by_name("Debian Palmtree")
            # Get the post installation script for palmtree workflow
            post_install_script = \
                self.postInstallScriptsManager.get_palmtree_script(self.port)

        # Select the security group called "vm_jdoe_sec_group"
        list_sec_group = []
        vm_jdoe_sec_group = self.securityGroupsManager\
            .get_all_security_groups()[0]
        list_sec_group.append(vm_jdoe_sec_group.name)

        # Select "test_key" in the list of keypairs
        test_key = self.keypairsManager.get_all_keypairs()[0].name

        # Select best suitable flavor depending on the job and the green value
        flavor = self.flavorManager.which_flavor_for_job(job, self.green_value)

        logger.info(self.name + ": will create " +
                    str(job.get_nb_instances()) + " instances for job " +
                    job.get_name())

        for x in range(0, job.get_nb_instances()):
            # Select best suitable host depending on the selected flavor
            host = self.hostManager.which_host_for_flavor(flavor)
            if host is None:
                logger.error("Unable to find a host. Leaving...")
                sys.exit()
            host_zone = host[len(host)-1]

            # Lock during instance creation to avoid while turing on a host
            self.hostManager.lock.acquire()
            # Create instance with this flavor
            instance = self.instanceManager.create_instance(
                name=self.name+'_'+job.get_name()+'_'+str(x),
                image=image,
                flavor=flavor,
                availability_zone=host_zone,
                keypair=test_key,
                sec_groups=list_sec_group,
                script=post_install_script)
            self.hostManager.lock.release()

            logger.info(self.name + ': instance ' + job.get_name() +
                        '_' + str(x) + ' created')

            # Save the instance id in the job description
            job.add_instance(instance)
            job.save_zone_for_instance(instance.id, host_zone, job.get_name())

            # Trick to allow OpenStack to update its variables
            time.sleep(4)

        logger.info(self.name + ": " + str(job.get_nb_instances()) +
                    " instances have been created")

        self.hostManager.turn_off_empty_hosts()

    def _prepare_frieda(self, job):
        """ Create the FRIEDA configuration file for each job

        :param job: Job for which to create FRIEDA files
        :return:
        """
        # Get name of the flavor used for this job
        first_instance = self.instanceManager\
            .get_instance(job.get_instances()[0].id)
        flavor = self.flavorManager\
            .get_specific_flavor(first_instance.flavor.get('id'))
        flavor_name = self.flavorManager.get_flavor_name(flavor)

        if self._create_yaml_conf(job, flavor_name) is None:
            logger.error("Cannot continue if the YAML file does not exist")
            sys.exit("Failed at _prepare_frieda")

        # Create output directory if needed and not created yet
        directory = job.get_output_dir()
        if directory != '' and not os.path.exists(directory):
            try:
                os.makedirs(directory)
                # Give folder's rights to the user 'upload'
                os.system("chown upload " + directory)
            except Exception as e:
                logger.error(e)

        logger.info(self.name + ": " + job.get_name() +
                    " has its frieda yaml file ready")

    def _prepare_floating_ips(self, job):
        """ Give a floating IP to all instances of the current job

        :param job: Job to which instances need floating IP
        :return:
        """
        instances = job.get_instances()
        for instance in instances:
            self.floatingIpManager.add_floating_ip_to_instance(instance)

    def _execute_frieda(self, job):
        """ Execute the `frieda` command for the job in parameter

        :param job: Job to execute with FRIEDA
        :return:
        """
        master_id = self.master_instance.id
        worker_ids = [worker.id for worker in job.get_instances()]
        job_file = common.WORKFLOWS_PATH + self.name + "/" + \
            job.get_name() + ".yaml"

        # Command and arguments
        command = "frieda"
        arg_frieda_type = "all"
        arg_frieda_yaml1 = "-c"
        arg_frieda_yaml2 = job_file
        arg_frieda_inst1 = "-i"
        arg_frieda_inst2 = master_id + "," + ",".join(worker_ids)

        print(command, arg_frieda_type, arg_frieda_yaml1, arg_frieda_yaml2,
              arg_frieda_inst1, arg_frieda_inst2)

        # Change directory for logs placement purpose
        os.chdir(common.WORKFLOWS_PATH + self.name + "/logs")
        try:
            subprocess.call([command, arg_frieda_type, arg_frieda_yaml1,
                             arg_frieda_yaml2, arg_frieda_inst1,
                             arg_frieda_inst2])
        except Exception as e:
            logger.error(e)

        self._save_frieda_logs(job.get_name())

        logger.info(self.name + ": execution of job " + job.get_name() +
                    " terminated")

        # Run post run script if set
        post_run = job.get_post_run()
        if post_run is not None:
            logger.info(self.name + ": execute post run bash script")
            os.system(post_run)

    def _delete_instances_of_job(self, job):
        """ Delete all instances of the job in parameter

        :param job: Job from which to delete instances
        :return:
        """
        workers = job.get_instances()

        # Save CPU util data
        logs_manager = LogsManager()
        index = 0
        for worker in workers:
            cpu_util_data = self.ceilometerManager.get_cpu_util(worker.id)
            job.save_cpu_util(worker.id, cpu_util_data)
            zone = job.get_zone_of_instance(worker.id)
            logs_manager.create_cpu_util_log_file(self.name, job.get_name(),
                                                  zone, worker.id,
                                                  cpu_util_data, index)
            index += 1
        logger.info(self.name + ": cpu util data of each instance of job " +
                    job.get_name() + " have been saved")

        for worker in workers:
            self.instanceManager.delete_instance(worker.id)
            time.sleep(1)
        logger.info(self.name + ": instances of job " + job.get_name() +
                    " have been deleted")

        self.hostManager.turn_off_empty_hosts()

    def _create_yaml_conf(self, job, flavor_name):
        """ Create the YAML file for a FRIEDA run

        :param job: Job from which to create the YAML configuration file
        :param flavor_name: Name of the instance type for the job
        :return:
        """
        sample = None
        filename = \
            common.WORKFLOWS_PATH + self.name + "/" + job.get_name() + ".yaml"

        # If the output directory is specified
        if job.get_output_dir() != '':
            try:
                sample = open(common.PATH + "frieda-sample.yaml", "r").read()
                sample = sample.replace("INST_TYPE", flavor_name)
                sample = sample.replace("SRC", job.get_source())
                sample = sample.replace("CMD", job.get_command())
                sample = sample.replace("INPUT_DIR", job.get_input_dir())
                sample = sample.replace("INPUT_TYPE", job.get_input_type())
                sample = sample.replace("OUTPUT_DIR", job.get_output_dir())
                sample = sample.replace("OUTPUT_TYPE", job.get_output_type())
                sample = sample.replace("WORKFLOW_NAME", self.name)
                controller_ip = str(socket.gethostbyname(socket.gethostname()))
                sample = sample.replace("CONTROLLER_IP", controller_ip)
            except Exception as e:
                logger.error(e)
                sys.exit("Failed at _create_yaml_conf")

        # If the output directory is not specified
        else:
            try:
                sample = open(common.PATH + "frieda-sample-no-output.yaml",
                              "r").read()
                sample = sample.replace("INST_TYPE", flavor_name)
                sample = sample.replace("SRC", job.get_source())
                sample = sample.replace("CMD", job.get_command())
                sample = sample.replace("INPUT_DIR", job.get_input_dir())
                sample = sample.replace("INPUT_TYPE", job.get_input_type())
                sample = sample.replace("WORKFLOW_NAME", self.name)
            except Exception as e:
                logger.error(e)
                sys.exit("Failed at _create_yaml_conf")

        if sample is not None:
            try:
                with open(filename, "w") as file:
                    file.write(sample)
                return 1
            except Exception as e:
                logger.error(e)
                logger.warning("The YAML file with the workflow description "
                               "must have the same name as its parent folder")
                return None
        else:
            logger.warning("sample variable is equal to None")
            return None

    def _create_master_instance(self):
        """ Create the master instance

        :return:
        """

        # Select the security group called "vm_jdoe_sec_group"
        list_sec_group = []
        vm_jdoe_sec_group = self.securityGroupsManager\
            .get_all_security_groups()[0]
        list_sec_group.append(vm_jdoe_sec_group.name)

        # Select "test_key" in the list of keypairs
        test_key = self.keypairsManager.get_all_keypairs()[0].name

        # Select the ubuntu-14.04-proxy image
        # todo generate suitable image
        ubuntu_image = self.imageManager\
            .get_image_by_name("Debian Jessie 64-bit")  # ubuntu-14.04-proxy

        # Select the medium flavor
        medium_flavor = self.flavorManager.get_specific_flavor(2)

        # Get post installation script for netcat
        netcat_script = self.postInstallScriptsManager\
            .get_netcat_script(self.port)
        
        # Select best suitable host depending on the selected flavor
        host = self.hostManager.which_host_for_flavor(medium_flavor)
        host_zone = host[len(host)-1]

        self.master_instance = self.instanceManager.create_instance(
            name=self.name + "_master",
            image=ubuntu_image,
            flavor=medium_flavor,
            availability_zone=host_zone,
            keypair=test_key,
            sec_groups=list_sec_group,
            script=netcat_script)

        self.jobManager.save_master_instance_id(self.master_instance.id)
        self.jobManager.save_master_zone(host_zone)

        logger.info(self.name + ": master instance created")

        # Trick to allow OpenStack to update its variables
        time.sleep(4)

    def _wait_for_instances(self, job):
        """ Listen on a specific port until all instances answer which means
        there are ready

        :param job: Job for which to wait for instances to be ready
        :return:
        """
        logger.info(self.name + ": waiting for instances of job " +
                    job.get_name() + " to be ready")

        # Number of instances
        remaining_instances = job.get_nb_instances()

        start_time = time.time()

        # Create the server socket
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.bind((socket.gethostname(), self.port))
        server_socket.listen(remaining_instances)

        # Wait for all the instances to send data
        while remaining_instances:
            (client_socket, address) = server_socket.accept()
            data = client_socket.recv(1024)
            logger.info("Instance " + data.decode('utf-8')[:-1] + " " +
                        str(address) + " ready in " +
                        str(time.time() - start_time) + " sec")
            remaining_instances -= 1
            client_socket.close()

        server_socket.shutdown(socket.SHUT_RDWR)
        server_socket.close()

        logger.info(self.name + ": all instances of job " + job.get_name() +
                    " are ready")

        # Avoid port conflict (happens sometimes)
        self.port += 1

    def _wait_for_master(self):
        """ Listen on a specific port until the master instance answers which
        means it is ready

        :return:
        """
        logger.info(self.name + ": waiting for master instance to be ready")

        start_time = time.time()

        # Create the server socket
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.bind((socket.gethostname(), self.port))
        server_socket.listen(1)  # wait for only 1 answer

        (client_socket, address) = server_socket.accept()
        data = client_socket.recv(1024)
        logger.info("Master instance " + data.decode('utf-8')[:-1] + " " +
                    str(address) + " ready in " +
                    str(time.time() - start_time) + " sec")
        client_socket.close()

        server_socket.shutdown(socket.SHUT_RDWR)
        server_socket.close()

        logger.info(self.name + ": master instance is ready")

        # Avoid port conflict (happens sometimes)
        self.port += 1

    def _save_frieda_logs(self, job_name):
        """ Move the log files to another directory to keep them safe from
        overwrites

        :param job_name: Name of the job that was just executed
        :return:
        """
        workflow_path = common.WORKFLOWS_PATH + self.name
        destination = workflow_path + '/logs_history/' + job_name + '/'

        # Create destination dir if does not exist
        if not os.path.exists(destination):
            os.makedirs(destination)

        try:
            # Try to move the log files to the destination folder
            subprocess.check_call(
                ['mv', workflow_path + '/logs/*', destination])
            subprocess.check_call(
                ['mv', workflow_path + '/state/* ', destination])
            logger.info(self.name + ': logs have been saved in ' + destination)
        except subprocess.CalledProcessError as e:
            logger.error(self.name + ': failed to save logs in ' + destination)
            logger.error(e.output)

    def _move_logs_to_graphs_gen_folder(self, folder_name):
        """ Move logs to graphs_gen folder

        :param folder_name: Name of the folder where to move the files
        :return:
        """
        destination = common.PATH + 'graphs_gen/' + folder_name + '/'

        # Create destination dir if does not exist
        if not os.path.exists(destination):
            os.makedirs(destination)

        if not os.path.exists(destination + self.name):
            os.makedirs(destination + self.name)

        workflow_path = common.WORKFLOWS_PATH + self.name
        try:
            subprocess.check_call(['mv', workflow_path +
                                   '/logs_history/master/cpu_util-master.log ',
                                   destination + self.name])

            jobs = self.jobManager.get_jobs()
            for job in jobs:
                job_name = job.get_name()
                subprocess.check_call(['mv', workflow_path + '/logs_history/' +
                                       job_name + '/cpu_util-* ', destination +
                                       self.name])
        except subprocess.CalledProcessError as e:
            logger.error(e.output)

    def _clean_execution(self):
        """ Clean the execution
        Remove the useless files created during the execution

        :return:
        """
        workflow_name = self.name

        os.system('rm ' + common.WORKFLOWS_PATH + workflow_name + '/step*')
        os.system('rm ' + common.WORKFLOWS_PATH + workflow_name + '/app*')
        os.system('rm ' + common.WORKFLOWS_PATH + workflow_name + '/no')
        os.system('rm -r ' + common.WORKFLOWS_PATH + workflow_name + '/state')
